// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TDS_MyMovementComponent_generated_h
#error "MyMovementComponent.generated.h already included, missing '#pragma once' in MyMovementComponent.h"
#endif
#define TDS_MyMovementComponent_generated_h

#define tds_Source_TDS_Character_MyMovementComponent_h_14_SPARSE_DATA
#define tds_Source_TDS_Character_MyMovementComponent_h_14_RPC_WRAPPERS
#define tds_Source_TDS_Character_MyMovementComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define tds_Source_TDS_Character_MyMovementComponent_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMyMovementComponent(); \
	friend struct Z_Construct_UClass_UMyMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UMyMovementComponent, UMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TDS"), NO_API) \
	DECLARE_SERIALIZER(UMyMovementComponent)


#define tds_Source_TDS_Character_MyMovementComponent_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUMyMovementComponent(); \
	friend struct Z_Construct_UClass_UMyMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UMyMovementComponent, UMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TDS"), NO_API) \
	DECLARE_SERIALIZER(UMyMovementComponent)


#define tds_Source_TDS_Character_MyMovementComponent_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMyMovementComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyMovementComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyMovementComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyMovementComponent(UMyMovementComponent&&); \
	NO_API UMyMovementComponent(const UMyMovementComponent&); \
public:


#define tds_Source_TDS_Character_MyMovementComponent_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMyMovementComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyMovementComponent(UMyMovementComponent&&); \
	NO_API UMyMovementComponent(const UMyMovementComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyMovementComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyMovementComponent)


#define tds_Source_TDS_Character_MyMovementComponent_h_14_PRIVATE_PROPERTY_OFFSET
#define tds_Source_TDS_Character_MyMovementComponent_h_11_PROLOG
#define tds_Source_TDS_Character_MyMovementComponent_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	tds_Source_TDS_Character_MyMovementComponent_h_14_PRIVATE_PROPERTY_OFFSET \
	tds_Source_TDS_Character_MyMovementComponent_h_14_SPARSE_DATA \
	tds_Source_TDS_Character_MyMovementComponent_h_14_RPC_WRAPPERS \
	tds_Source_TDS_Character_MyMovementComponent_h_14_INCLASS \
	tds_Source_TDS_Character_MyMovementComponent_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define tds_Source_TDS_Character_MyMovementComponent_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	tds_Source_TDS_Character_MyMovementComponent_h_14_PRIVATE_PROPERTY_OFFSET \
	tds_Source_TDS_Character_MyMovementComponent_h_14_SPARSE_DATA \
	tds_Source_TDS_Character_MyMovementComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	tds_Source_TDS_Character_MyMovementComponent_h_14_INCLASS_NO_PURE_DECLS \
	tds_Source_TDS_Character_MyMovementComponent_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TDS_API UClass* StaticClass<class UMyMovementComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID tds_Source_TDS_Character_MyMovementComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
