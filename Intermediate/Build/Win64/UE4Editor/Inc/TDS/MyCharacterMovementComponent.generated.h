// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TDS_MyCharacterMovementComponent_generated_h
#error "MyCharacterMovementComponent.generated.h already included, missing '#pragma once' in MyCharacterMovementComponent.h"
#endif
#define TDS_MyCharacterMovementComponent_generated_h

#define tds_Source_TDS_Character_MyCharacterMovementComponent_h_15_SPARSE_DATA
#define tds_Source_TDS_Character_MyCharacterMovementComponent_h_15_RPC_WRAPPERS
#define tds_Source_TDS_Character_MyCharacterMovementComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define tds_Source_TDS_Character_MyCharacterMovementComponent_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMyCharacterMovementComponent(); \
	friend struct Z_Construct_UClass_UMyCharacterMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UMyCharacterMovementComponent, UCharacterMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TDS"), NO_API) \
	DECLARE_SERIALIZER(UMyCharacterMovementComponent)


#define tds_Source_TDS_Character_MyCharacterMovementComponent_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUMyCharacterMovementComponent(); \
	friend struct Z_Construct_UClass_UMyCharacterMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UMyCharacterMovementComponent, UCharacterMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TDS"), NO_API) \
	DECLARE_SERIALIZER(UMyCharacterMovementComponent)


#define tds_Source_TDS_Character_MyCharacterMovementComponent_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMyCharacterMovementComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyCharacterMovementComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyCharacterMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyCharacterMovementComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyCharacterMovementComponent(UMyCharacterMovementComponent&&); \
	NO_API UMyCharacterMovementComponent(const UMyCharacterMovementComponent&); \
public:


#define tds_Source_TDS_Character_MyCharacterMovementComponent_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMyCharacterMovementComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyCharacterMovementComponent(UMyCharacterMovementComponent&&); \
	NO_API UMyCharacterMovementComponent(const UMyCharacterMovementComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyCharacterMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyCharacterMovementComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyCharacterMovementComponent)


#define tds_Source_TDS_Character_MyCharacterMovementComponent_h_15_PRIVATE_PROPERTY_OFFSET
#define tds_Source_TDS_Character_MyCharacterMovementComponent_h_12_PROLOG
#define tds_Source_TDS_Character_MyCharacterMovementComponent_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	tds_Source_TDS_Character_MyCharacterMovementComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	tds_Source_TDS_Character_MyCharacterMovementComponent_h_15_SPARSE_DATA \
	tds_Source_TDS_Character_MyCharacterMovementComponent_h_15_RPC_WRAPPERS \
	tds_Source_TDS_Character_MyCharacterMovementComponent_h_15_INCLASS \
	tds_Source_TDS_Character_MyCharacterMovementComponent_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define tds_Source_TDS_Character_MyCharacterMovementComponent_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	tds_Source_TDS_Character_MyCharacterMovementComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	tds_Source_TDS_Character_MyCharacterMovementComponent_h_15_SPARSE_DATA \
	tds_Source_TDS_Character_MyCharacterMovementComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	tds_Source_TDS_Character_MyCharacterMovementComponent_h_15_INCLASS_NO_PURE_DECLS \
	tds_Source_TDS_Character_MyCharacterMovementComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TDS_API UClass* StaticClass<class UMyCharacterMovementComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID tds_Source_TDS_Character_MyCharacterMovementComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
