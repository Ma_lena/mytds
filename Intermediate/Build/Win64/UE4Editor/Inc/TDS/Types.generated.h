// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TDS_Types_generated_h
#error "Types.generated.h already included, missing '#pragma once' in Types.h"
#endif
#define TDS_Types_generated_h

#define tds_Source_TDS_FuncLibrary_Types_h_21_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCharecterSpeed_Statics; \
	TDS_API static class UScriptStruct* StaticStruct();


template<> TDS_API UScriptStruct* StaticStruct<struct FCharecterSpeed>();

#define tds_Source_TDS_FuncLibrary_Types_h_37_SPARSE_DATA
#define tds_Source_TDS_FuncLibrary_Types_h_37_RPC_WRAPPERS
#define tds_Source_TDS_FuncLibrary_Types_h_37_RPC_WRAPPERS_NO_PURE_DECLS
#define tds_Source_TDS_FuncLibrary_Types_h_37_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTypes(); \
	friend struct Z_Construct_UClass_UTypes_Statics; \
public: \
	DECLARE_CLASS(UTypes, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TDS"), NO_API) \
	DECLARE_SERIALIZER(UTypes)


#define tds_Source_TDS_FuncLibrary_Types_h_37_INCLASS \
private: \
	static void StaticRegisterNativesUTypes(); \
	friend struct Z_Construct_UClass_UTypes_Statics; \
public: \
	DECLARE_CLASS(UTypes, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TDS"), NO_API) \
	DECLARE_SERIALIZER(UTypes)


#define tds_Source_TDS_FuncLibrary_Types_h_37_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTypes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTypes) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTypes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTypes); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTypes(UTypes&&); \
	NO_API UTypes(const UTypes&); \
public:


#define tds_Source_TDS_FuncLibrary_Types_h_37_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTypes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTypes(UTypes&&); \
	NO_API UTypes(const UTypes&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTypes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTypes); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTypes)


#define tds_Source_TDS_FuncLibrary_Types_h_37_PRIVATE_PROPERTY_OFFSET
#define tds_Source_TDS_FuncLibrary_Types_h_34_PROLOG
#define tds_Source_TDS_FuncLibrary_Types_h_37_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	tds_Source_TDS_FuncLibrary_Types_h_37_PRIVATE_PROPERTY_OFFSET \
	tds_Source_TDS_FuncLibrary_Types_h_37_SPARSE_DATA \
	tds_Source_TDS_FuncLibrary_Types_h_37_RPC_WRAPPERS \
	tds_Source_TDS_FuncLibrary_Types_h_37_INCLASS \
	tds_Source_TDS_FuncLibrary_Types_h_37_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define tds_Source_TDS_FuncLibrary_Types_h_37_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	tds_Source_TDS_FuncLibrary_Types_h_37_PRIVATE_PROPERTY_OFFSET \
	tds_Source_TDS_FuncLibrary_Types_h_37_SPARSE_DATA \
	tds_Source_TDS_FuncLibrary_Types_h_37_RPC_WRAPPERS_NO_PURE_DECLS \
	tds_Source_TDS_FuncLibrary_Types_h_37_INCLASS_NO_PURE_DECLS \
	tds_Source_TDS_FuncLibrary_Types_h_37_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TDS_API UClass* StaticClass<class UTypes>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID tds_Source_TDS_FuncLibrary_Types_h


#define FOREACH_ENUM_EMOVEMENTSTATE(op) \
	op(EMovementState::Aim_State) \
	op(EMovementState::Walk_State) \
	op(EMovementState::Run_State) \
	op(EMovementState::AimWalk_State) \
	op(EMovementState::SprintRun_State) 

enum class EMovementState : uint8;
template<> TDS_API UEnum* StaticEnum<EMovementState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
